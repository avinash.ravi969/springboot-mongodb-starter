/**
 * 
 */
package com.springboot.mongodb.starter.enums;

/**
 * @author Avinash
 *
 */
public interface BaseEnum {
	public long getClassification();

	public Long getCode();

	public String getDisplayName();
}
