/**
 * 
 */
package com.springboot.mongodb.starter.documents;

import javax.persistence.Convert;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.springboot.mongodb.starter.converters.TypeConverter;
import com.springboot.mongodb.starter.enums.Type;

/**
 * @author Avinash
 *
 */
@Document(collection = "fileInformation")
public class FileInformation {
	
	@Id private String id;

	//@Convert(converter = TypeConverter.class)
	public Type type;
	
	public String name;
	
	private Object data;
	
	 @DBRef
	 public Person person;

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the person
	 */
	public Person getPerson() {
		return person;
	}

	/**
	 * @param person the person to set
	 */
	public void setPerson(Person person) {
		this.person = person;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	
}
