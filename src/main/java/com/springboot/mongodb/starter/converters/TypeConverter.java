/**
 * 
 */
package com.springboot.mongodb.starter.converters;

import org.bson.conversions.Bson;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.mongodb.core.convert.MongoTypeMapper;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty;
import org.springframework.data.util.TypeInformation;

import com.mongodb.DBRef;
import com.springboot.mongodb.starter.enums.Type;

/**
 * @author Avinash
 *
 */
public class TypeConverter implements Converter<String, Type>  {

	@Override
	public Type convert(String dbData) {
		return Type.valueOf(dbData);
	}
	
	
}
