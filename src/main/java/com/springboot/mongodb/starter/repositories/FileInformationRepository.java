/**
 * 
 */
package com.springboot.mongodb.starter.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.springboot.mongodb.starter.documents.FileInformation;

/**
 * @author Avinash
 *
 */
public interface FileInformationRepository extends MongoRepository<FileInformation, String>{

}
