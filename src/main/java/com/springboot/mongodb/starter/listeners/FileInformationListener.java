/**
 * 
 */
package com.springboot.mongodb.starter.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import com.springboot.mongodb.starter.documents.FileInformation;

/**
 * @author Avinash
 *
 */
@Component
public class FileInformationListener extends AbstractMongoEventListener<FileInformation> {
	
    @Autowired
    private MongoOperations mongoOperations;
 
    @Override
    public void onBeforeConvert(BeforeConvertEvent<FileInformation> event) { 
        Object source = event.getSource(); 
        if ((source instanceof FileInformation) && (((FileInformation) source).getName() != null)) { 
            System.out.println(((FileInformation) source).getName());
        }
    }
}
