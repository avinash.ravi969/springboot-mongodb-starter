/**
 * 
 */
package com.springboot.mongodb.starter.enums;

import java.util.HashMap;



/**
 * @author Avinash
 *
 */
public enum Type implements BaseEnum{
	
	FILE_ADD(9001,"File Add"), FILE_DELETE(9002, "File Delete"), FOLDER_ADD(9003, "Folder Add");
	
	private Long code;

	private String displayName;

	private static HashMap<Long, Type> typeMap = new HashMap<Long, Type>(Type.values().length);
	
	static {
		for (Type type : Type.values()) {
			typeMap.put(type.code, type);
		}
	}
	
	Type(long code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public long getClassification() {
		return 9000;
	}

	@Override
	public Long getCode() {
		return code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public static Type getInstanceFromCode(long code) {
		return typeMap.get(code);
	}
	
}
